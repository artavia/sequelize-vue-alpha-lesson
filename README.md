# Sequelize Vue Alpha Lesson

## Description
Taking Sequelize for a test run with VueJS and Express in the front and back ends respectively.

### Processing and/or completion date(s)
June 26, 2020 to June 27, 2020

## Attribution:
This exercise was originally done by the folks at [StackAbuse](https://stackabuse.com/using-sequelize-js-and-sqlite-in-an-express-js-app/ "link to stackabuse"). 

This project is referred to as Alpha since everything is on the backend and it has no routing in terms of VueJS. Once successful with refactoring the Alpha to my liking, Beta will expand on those lessons learned while formulating the Alpha version and will definitively have a client folder and a server folder mainly because of the employment of the **vue create** directive (or at least that is the objective anyways&hellip;). Lord permitting this will all work out according to plan. 

## This is one of many baby steps to cure personal shortfalls in deciphering Sequelize errors
I have in recent weeks attempted to complete the 
[ApolloGraphQL fullstack tutorial](https://www.apollographql.com/docs/tutorial/introduction/ "link to tutorial"). In order to understand an error I encountered in **that** lesson, I need to understand Sequelize and how it relates to the bigger picture. **sequelize-cli** was an important part, too, and laid the groundwork for future experimentation with more organic projects developed without the aid of **sequelize-cli**.

Help me if you are able. Otherwise, what some of you mean for evil against me God means it for good and render your evil deeds impotent. God has a plan and an agenda for me. I hope you can say the same.

## God bless!