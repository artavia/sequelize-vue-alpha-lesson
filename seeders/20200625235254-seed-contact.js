'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

    return await queryInterface.bulkInsert('Contacts', [
      {
        firstName: 'Monkey'
        , lastName: 'D. Luffy'
        , phone: '111-222-3333'
        , email: "luffy@strawhats.com"
        , createdAt: new Date().toDateString()
        , updatedAt: new Date().toDateString()
      }
      , {
        firstName: 'Roronoa'
        , lastName: 'Zoro'
        , phone: '222-333-4444'
        , email: "zoro@strawhats.com"
        , createdAt: new Date().toDateString()
        , updatedAt: new Date().toDateString()
      }
      , {
        firstName: 'Melanine'
        , lastName: 'Nami'
        , phone: '333-444-5555'
        , email: "nami@strawhats.com"
        , createdAt: new Date().toDateString()
        , updatedAt: new Date().toDateString()
      }
      , {
        firstName: 'Usopp'
        , lastName: 'Sogekingu'
        , phone: '555-777-8888'
        , email: "usopp@strawhats.com"
        , createdAt: new Date().toDateString()
        , updatedAt: new Date().toDateString()
      }
      , {
        firstName: 'Sanji'
        , lastName: 'Vinsmoke'
        , phone: '777-888-9999'
        , email: "sanji@strawhats.com"
        , createdAt: new Date().toDateString()
        , updatedAt: new Date().toDateString()
      }
      , {
        firstName: 'Tony Tony'
        , lastName: 'Chopper'
        , phone: '888-999-0000'
        , email: "chopper@strawhats.com"
        , createdAt: new Date().toDateString()
        , updatedAt: new Date().toDateString()
      }
      , {
        firstName: 'Nico'
        , lastName: 'Robin'
        , phone: '999-111-2222'
        , email: "robin@strawhats.com"
        , createdAt: new Date().toDateString()
        , updatedAt: new Date().toDateString()
      }
      , {
        firstName: 'Franky'
        , lastName: 'Cutty Flam'
        , phone: '000-111-2222'
        , email: "franky@strawhats.com"
        , createdAt: new Date().toDateString()
        , updatedAt: new Date().toDateString()
      }
      , {
        firstName: 'Brooke'
        , lastName: 'Burrukku'
        , phone: '111-222-6666'
        , email: "brooke@strawhats.com"
        , createdAt: new Date().toDateString()
        , updatedAt: new Date().toDateString()
      }
    ], {});
    

  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    return await queryInterface.bulkDelete('Contacts', null, {});
  }
};
