// =============================================
// LOCAL DEVELOPMENT ENV VARS SETUP
// =============================================
if( process.env.NODE_ENV !== 'production' ){ 
  const dotenv = require('dotenv'); 
  dotenv.config();
}

// =============================================
// process.env SETUP
// =============================================
const { NODE_ENV , PORT } = process.env;

// =============================================
// Establish the NODE_ENV toggle settings
// ==============================================
const IS_PRODUCTION_NODE_ENV = NODE_ENV === 'production'; // true
const IS_DEVELOPMENT_NODE_ENV = NODE_ENV === 'development'; // true

// =============================================
// BASE SETUP
// =============================================
const express = require( 'express' );
const app = express(); 


// =============================================
// essential boilerplate
// =============================================
const path = require('path');

// =============================================
// optional boilerplate
// =============================================

const helmet = require( 'helmet' ); // https://expressjs.com/en/advanced/best-practice-security.html

const logger = require('morgan');
const createError = require('http-errors');
const serveStatic = require('serve-static');

const cors = require("cors");

// =============================================
// ROUTER SETUP
// =============================================
const apiRouter = require("./routes/contactApi");

// =============================================
// APP SETUP - SET PARAMS
// =============================================

// ---------------------------------------------
// | trust proxy setting                                                |
// | this permits the assignment of secure cookies which is COULD CHOKE
// | because of, FOR EXAMPLE, express-session
// ---------------------------------------------
// app.enable( 'trust proxy' , 1 );

// ---------------------------------------------
// | Server-side technology information                                 |
// | Remove the `X-Powered-By` response header that
// | contributes to header bloat and can unnecessarily expose vulnerabilities
// ---------------------------------------------
app.disable( 'x-powered-by' );

// ---------------------------------------------
// | ETags                                                              |
// | PERFORMANCE helper function
// | Remove `ETags` as resources are sent with far-future expires headers
// | https://developer.yahoo.com/performance/rules.html#etags
// ---------------------------------------------
app.set( 'etag' , false );

// =============================================
// USE PARAMS - FINER DETAILS
// =============================================
app.use( cors() );
// See the react auth blog in which cors is required for access

/* app.use((req, res, next) => { 
  res.setHeader('Access-Control-Allow-Origin', '*');
  next();
});  */

app.use(logger('dev'));

// app.use( bodyParser.json() );
app.use( express.json() ); // for parsing application/json
app.use( express.urlencoded( { extended: true } ) ); // for parsing application/x-www-form-urlencoded

// ---------------------------------------------
// | helmet                                                              |
// ---------------------------------------------
app.use( helmet() ); 
// app.use( helmet.noCache() ); // https://helmetjs.github.io/docs/nocache/ 

// =============================================
// Serve static files from the organic Vue app
// =============================================
if( ( IS_PRODUCTION_NODE_ENV === true ) || ( IS_DEVELOPMENT_NODE_ENV === true ) ){
  app.use( express.static( path.join( __dirname, 'static' ) ) );
}

// =============================================
// | Expires headers                                                    |
// | PERFORMANCE helper function
// | https://gist.github.com/tagr/170627486377551831de
// | https://devcenter.heroku.com/articles/increasing-application-performance-with-http-cache-headers
// =============================================

const newCustom_Enable_Expires_Headers = ( res, path ) => {
  
  if( ( serveStatic.mime.lookup( path ) === 'text/cache-manifest' ) || ( serveStatic.mime.lookup( path ) === 'application/xml' ) || ( serveStatic.mime.lookup( path ) === 'application/json' ) ){  // zero seconds
    // res.set( 'Cache-Control' , 'private, max-age=0' );
    var secs = 0;
    var expr = 'public, max-age='+ '' + secs +'';
    res.set( 'Cache-Control' , expr );
    // res.set( 'Expires', new Date(Date.now() + 0 ).toUTCString() );
    res.set( 'Expires', new Date(Date.now() + (1000 * secs) ).toUTCString() ); 
  } 
  else 
  if( ( serveStatic.mime.lookup( path ) === 'application/rss+xml' ) ){ // 1 hour (86400 / 24)  
    // res.set( 'Cache-Control' , 'private, max-age=3600' );
    var secs = 3600;
    var expr = 'public, max-age='+ '' + secs +'';
    res.set( 'Cache-Control' , expr );
    // res.set( 'Expires', new Date(Date.now() + 3600000 ).toUTCString() );
    res.set( 'Expires', new Date(Date.now() + (1000 * secs) ).toUTCString() ); 
  }
  
  else 
  if( ( serveStatic.mime.lookup( path ) === 'image/x-icon' ) ){ // 1 week (86400 * 7)  
    // res.set( 'Cache-Control' , 'private, max-age=604800' );
    var secs = 604800;
    var expr = 'public, max-age='+ '' + secs +'';
    res.set( 'Cache-Control' , expr );
    // res.set( 'Expires', new Date(Date.now() + 604800000 ).toUTCString() );
    res.set( 'Expires', new Date(Date.now() + (1000 * secs) ).toUTCString() ); 
  }
  else

  if( ( serveStatic.mime.lookup( path ) === 'font/woff' ) || ( serveStatic.mime.lookup( path ) === 'font/woff2' ) || ( serveStatic.mime.lookup( path ) === 'application/vnd.ms-fontobject' ) || ( serveStatic.mime.lookup( path ) === 'application/font-sfnt' ) || ( serveStatic.mime.lookup( path ) === 'application/octet-stream' ) || ( serveStatic.mime.lookup( path ) === 'video/x-f4v' ) || ( serveStatic.mime.lookup( path ) === 'audio/mp4a-latm' ) || ( serveStatic.mime.lookup( path ) === 'audio/ogg' ) || ( serveStatic.mime.lookup( path ) === 'video/mp4' ) || ( serveStatic.mime.lookup( path ) === 'video/ogg' ) || ( serveStatic.mime.lookup( path ) === 'video/webm' ) || ( serveStatic.mime.lookup( path ) === 'image/bmp' ) || ( serveStatic.mime.lookup( path ) === 'image/gif' ) || ( serveStatic.mime.lookup( path ) === 'image/jpeg' ) || ( serveStatic.mime.lookup( path ) === 'image/png' ) || ( serveStatic.mime.lookup( path ) === 'image/svg+xml' ) || ( serveStatic.mime.lookup( path ) === 'image/webp' )  ){ // 30 days (86400 * 30 )
    
    // res.set( 'Cache-Control' , 'private, max-age=2592000' );
    var secs = 2592000;
    var expr = 'public, max-age='+ '' + secs +'';
    res.set( 'Cache-Control' , expr );
    // res.set( 'Expires', new Date(Date.now() + 2592000000 ).toUTCString() );
    res.set( 'Expires', new Date(Date.now() + (1000 * secs) ).toUTCString() ); 
  }

  // webm|bmp|gif|jpg|jpeg|png|svgz?|webp

  else 
  if( ( serveStatic.mime.lookup( path ) === 'text/css' ) || ( serveStatic.mime.lookup( path ) === 'application/javascript' ) ){
    
    // res.set( 'Cache-Control' , 'private, max-age=31536000' );
    var secs = 31536000;
    var expr = 'public, max-age='+ '' + secs +'';
    res.set( 'Cache-Control' , expr );
    // res.set( 'Expires', new Date(Date.now() + 31536000000 ).toUTCString() );
    res.set( 'Expires', new Date(Date.now() + (1000 * secs) ).toUTCString() ); 
  }

};

if( (IS_PRODUCTION_NODE_ENV === true) || ( IS_DEVELOPMENT_NODE_ENV === true  ) ){
  app.use( serveStatic( path.join( __dirname , 'static' ) , {
    setHeaders: newCustom_Enable_Expires_Headers
  } ) );  
}

// =============================================
// APPLY THE ROUTES TO THE APP
// =============================================
app.use( "/api" , apiRouter );

// =============================================
// The "catchall" handler that will handle all requests in order to return the organic Vue app's index.html file.
// =============================================
const catchAllGET = ( req, res, next ) => {
  res.sendFile( path.join( __dirname , "static", "index.html" ) );
};

if( ( IS_PRODUCTION_NODE_ENV === true ) || ( IS_DEVELOPMENT_NODE_ENV === true ) ){
  app.get( '*' , catchAllGET );
}

// =============================================
// ERROR HANDLING - SERVER SIDE CONTINGENCIES
// =============================================

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  res.status(err.status || 500);
  // res.render('error'); // DNE - do not render "error"
});



app.listen( PORT , () => { 

  // console.log( `Server is running on port ${PORT}` ); 
  // console.log( "PORT" , PORT );
    
  if( IS_PRODUCTION_NODE_ENV === true ){
    console.log( `Backend is running, baby.`);
  }
  if( IS_DEVELOPMENT_NODE_ENV === true ){
    console.log( `Production backend has started at http://127.0.0.1:${PORT} .`);
  }
  if( IS_PRODUCTION_NODE_ENV === false && IS_DEVELOPMENT_NODE_ENV === false ){ 
    console.log(`Development backend is now testing at port ${PORT} . Run and visit your frontend!`);
  }
  
} ); // PORT, hostname, backlog (null), cb 