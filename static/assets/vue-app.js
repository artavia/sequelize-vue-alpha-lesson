( () => {

  const AddUpdateContact = {
    props: [ 'contact' , 'title' ]
    
    , data: function() {
      return {
        id: this.contact ? this.contact.id : null
        , firstName: this.contact ? this.contact.firstName : ""
        , lastName: this.contact ? this.contact.lastName : ""
        , phone: this.contact ? this.contact.phone : ""
        , email: this.contact ? this.contact.email : ""
      };
    }

    , methods: {
      save() {
        this.$emit( 'save-contact', {
          id: this.id
          , firstName: this.firstName
          , lastName: this.lastName
          , phone: this.phone 
          , email: this.email 
        } );
        if(!this.id){
          this.firstName = "";
          this.lastName = "";
          this.phone = "";
          this.email = "";
        }
      }
      // ,
    }
    , template: `
      <form class="form" @submit.prevent="save">
        <h3 class="subtitle">{{ title }}</h3>
        
        <div class="field">
          <label>First Name</label>
          <div class="control">
            <input class="input" type="text" v-model="firstName" placeholder="John" />
          </div>
        </div>

        <div class="field">
          <label>Last Name</label>
          <div class="control">
            <input class="input" type="text" v-model="lastName" placeholder="Doe" />
          </div>
        </div>

        <div class="field">
          <label>Phone</label>
          <div class="control">
            <input class="input" type="text" v-model="phone" placeholder="111-222-3333"/>
          </div>
        </div>

        <div class="field">
          <label>Email</label>
          <div class="control">
            <input class="input" type="text" v-model="email" placeholder="johndoe@email.com" />
          </div>
        </div>

        <div class="field">
          <div class="control">
            <input class="button is-success" type="submit" :disabled="email.length == 0 || phone.length == 0 || lastName.length == 0 || firstName.length == 0" value="Save">
          </div>
        </div>

      </form>
    `
  };

  const Contact = {
    props: [ "contact" ]
    , components: { "add-update-contact": AddUpdateContact }
    , data: function(){
      return {
        showDetail: false
      };
    }
    , methods: {
      onAddOrUpdateContact( contact ) {
        this.$emit( 'save-contact' , contact );
      }
      , deleteContact( contact ){
        this.$emit( 'delete-contact' , contact );
      }
    }
    , template: `
      <div class="card">
        <header class="card-header">
          <p @click="showDetail = !showDetail" class="card-header-title">
            {{ contact.firstName }} {{ contact.lastName }}
          </p>
          <a class="card-header-icon" @click.stop="deleteContact(contact)">
            <span class="icon">
              <i class="fa fa-trash-alt"></i>
            </span>
          </a>
        </header>
        <div v-show="showDetail" class="card-content">
          <add-update-contact title="Details" :contact="contact" @save-contact="onAddOrUpdateContact" />
        </div>
      </div>
    `
  };
  
  const app = new Vue(
    {
      el: "#app"
      , components: {
        "contact": Contact, "add-update-contact": AddUpdateContact
      }
      , data: {
        contacts: []
        , apiURL: "http://localhost:3000/api/contacts"
        , error: false
      }
      , methods: {
        onAddOrUpdateContact(contact){
          
          // console.log( 'either... or... ' , contact );

          if( contact.id ){
            this.updateContact( contact );
          }
          else {
            this.addContact( contact );
          }
        }
        , addContact( contact ){
          
          // console.log( 'adding...' , contact );

          return axios.post( this.apiURL, contact ).then( (response) => {

            // console.log( "response.data" , response.data );
            // console.log( "response" , response );

            const copy = this.contacts.slice();
            copy.push( response.data );
            this.contacts = copy;
          } );
        }
        , updateContact( contact ){
          
          // console.log( 'updating...' , contact );

          return axios.put( `${ this.apiURL }/${ contact.id }` , contact ).then( (response) => {
            
            // console.log( "response.data" , response.data );
            // console.log( "response" , response );

            const copy = this.contacts.slice();
            const idx = copy.findIndex( (c) => c.id === response.data.id );
            copy[ idx ] = response.data;
            
            this.contacts = copy;
          } );
        }

        , deleteContact( contact ){
          
          // console.log( 'deleting...' , contact );

          return axios.delete( `${ this.apiURL }/${ contact.id }` ).then( (response) => {

            // console.log( "response.data" , response.data );
            // console.log( "response" , response );

            let copy = this.contacts.slice();
            // console.log( "copy" , copy );

            const idx = copy.findIndex( (c) => c.id === response.data.id );
            // console.log( "idx", idx );

            copy.splice( idx, 1 );
            // console.log( "copy" , copy );

            this.contacts = copy;

            this.getAllContacts();

          } );
        }
        
        , getAllContacts(){
          this.error = false;
  
          return axios.get( this.apiURL ).then( (response) => {
            
            // console.log( "response" , response );
            // console.log( "response.data" , response.data );
            
            // ONLY A TEST...
            if( response.data.errormessage !== undefined ){
              console.log( "response.data.errormessage" , response.data.errormessage );
              this.error = response.data.errormessage;
            }
            else {
              this.contacts = response.data;
            }
  
          } );
        }

      }
      , beforeMount(){
        return this.getAllContacts();
      }
    }
  );

} )();