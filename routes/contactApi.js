// BASE SETUP
// =============================================
const express = require("express");
const contactApiRouter = express.Router();

// new boilerplate
// =============================================
const compression = require( 'compression' );  // see https://expressjs.com/en/advanced/best-practice-performance.html 


// BOTH
// IMPORT MODEL DEFINITION 
// AND
// IMPORT DB MIDDLEWARE
// =============================================
const db = require("../models");

// =============================================
// COMPRESSION
// ==============================================
contactApiRouter.use( compression() );


// ROUTING ASSIGNMENTS
// =============================================
contactApiRouter.get( "/contacts" , ( req, res ) => {
  
  // TODO: retrieve contacts

  return db.Contact.findAll()
  .then( ( contacts ) => {
    
    // return res.send( contacts );
    return res.status(200).json( contacts ); 

    // ONLY A TEST...
    // return res.json( { errormessage: "Internal Error -- problem loading data." } );
    
  } )
  .catch( ( err ) => {
    
    console.log( `There was derrpage: ` , JSON.stringify( err, null, 2 ) );

    // return res.send( err );
    return res.status(400).json( { errormessage: "Internal Error -- problem loading data." } );

  } );

} );

contactApiRouter.post( "/contacts" , ( req, res ) => {
  
  const { firstName, lastName, phone, email } = req.body;

  // TODO: create contact

  return db.Contact.create( { firstName, lastName, phone, email } )
  .then( ( newcontact ) => {
    
    console.log( '>>>>>> New contact added: ' , newcontact );

    return res.status(200).json( newcontact );
    
    // return res.status(200).json( { message : "New contact has been added" } );

  } )
  .catch( ( err ) => { 
    
    console.log( `There was derrpage: ` , JSON.stringify( err, null, 2 ) ); 

    return res.status(400).json( { errormessage: "Error creating a new contact." } );
  } );

} );

contactApiRouter.delete( "/contacts/:id" , ( req, res ) => {
  
  // TODO: find and delete contact by id
  
  const id = parseInt( req.params.id );

  return db.Contact.findByPk(id)
  .then( ( contact ) => {
    // return contact.destroy();
    return contact.destroy( { where: {id : id } } );
  } )
  .then( () => {
    return res.status(200).json( id );
  } )
  .catch( ( err ) => { 
    
    console.log( `There was derrpage: ` , JSON.stringify( err, null, 2 ) ); 
  
    return res.status(400).json( { errormessage: "Error deleting contact." } );

  } );

} );

contactApiRouter.put( "/contacts/:id" , ( req, res ) => {
  
  // TODO: find and update contact by id
  const id = parseInt( req.params.id );
  
  return db.Contact.findByPk( id )
  .then( ( contact ) => {
    
    const { firstName, lastName, phone, email } = req.body;
    return contact.update( { firstName, lastName, phone, email } )
    .then( ( contact ) => {
      return res.status(200).json( contact );
    } )
    .catch( ( err ) => { 
      console.log( `There was derrpage: ` , JSON.stringify( err, null, 2 ) ); 
  
      return res.status(400).json( { errormessage: "Error updating existing contact." } );
      
    } );

  } );
  
} );

module.exports = contactApiRouter;